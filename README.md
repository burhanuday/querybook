# Querybook

The application is deployed at [https://querybook.vercel.app/](https://querybook.vercel.app/)

Querybook is a dummy web-based IDE for analysts to collaborate on queries, visualize data, and save results.

The application is built with React and TypeScript.

## Features

- Query editor
- Query result visualization for performance
- Query history
- Saved queries
- Export query results to CSV

## External libraries

- Zustand: Simple global state management for React
- Tanstack Table for React: Table component for React for handling large amounts of data
- Tanstack Virtual for React: Virtualized list utilities for React for handling large amounts of data
- Tailwind CSS: Utility-first CSS framework for basic styling
- React: JavaScript library for building user interfaces
- Export to CSV: Library for exporting data to CSV

## Speed

Find the attached Lighthouse report for the application. [Lighthouse Report](./lighthouse.pdf)

The network tab shows `DOMContentLoaded` to be 75ms and `load` to be 100ms.

The reason for overall fast load times and snappiness is due to the following:

1. No runtime CSS - TailwindCSS is generated at build time and is purged of unused CSS allowing for faster page loads compared to other CSS in JS solutions
2. Few dependencies - The application has few dependencies which means that there is less code to load
3. Lazy loading and code splitting - The application **CAN** split into chunks and loaded on demand. This means that the initial load is fast and the rest of the application is loaded on demand. This is not currently done due to it being a small application but can be done in the future. At the moment, `export-to-csv` library is loaded lazily only when the `Export` button is clicked on the query result page.

Note: Speed can be measured on different network and CPU conditions. The above is measured on a 4G network and a MacBook Pro M1.
