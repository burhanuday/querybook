type InputProps = React.ComponentPropsWithoutRef<"input"> & {
  label?: string;
};

export default function Input(props: InputProps) {
  return (
    <div>
      {props.label ? (
        <label
          htmlFor={props.id}
          className="block text-sm font-medium leading-6 text-gray-900"
        >
          {props.label}
        </label>
      ) : null}
      <div className="mt-2">
        <input
          className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600"
          {...props}
        />
      </div>
    </div>
  );
}
