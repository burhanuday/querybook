import { useQueryStore } from "../../store/query";
import ListItem from "./ListItem/list-item";

export default function Sidebar() {
  const { history, runQuery, savedQueries } = useQueryStore((state) => ({
    history: state.history,
    runQuery: state.runQuery,
    savedQueries: state.savedQueries,
  }));

  const handleQueryClicked = (query: string) => {
    runQuery(query);
  };

  return (
    <div className="p-5 w-full max-w-[20%] space-y-8 h-full flex-shrink-0 max-h-screen">
      <div className="h-1/2 overflow-y-auto">
        <h3 className="text-base font-semibold leading-7 text-gray-900">
          Available Queries
        </h3>
        <ul>
          {savedQueries.map((query, index) => (
            <ListItem
              key={index + query}
              text={query}
              onClick={() => handleQueryClicked(query)}
            />
          ))}
        </ul>
      </div>
      <div className="h-1/2 overflow-y-auto">
        <h3 className="text-base font-semibold leading-7 text-gray-900">
          History
        </h3>
        <ul className="">
          {history.map((query, index) => (
            <ListItem
              key={index + query}
              text={query}
              onClick={() => handleQueryClicked(query)}
            />
          ))}
        </ul>
      </div>
    </div>
  );
}
