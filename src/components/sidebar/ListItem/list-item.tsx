type ListItemProps = {
  text: string;
  onClick: () => void;
};

export default function ListItem({ text, onClick }: ListItemProps) {
  return (
    <li>
      <button
        onClick={onClick}
        className="inline-flex items-center text-left w-full px-4 py-2 mt-1 text-base text-gray-900 transition duration-500 ease-in-out transform rounded-lg focus:shadow-outline hover:bg-gray-100"
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 24 24"
          fill="none"
          stroke="currentColor"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
          className="w-4 h-4"
        >
          <polygon points="5 3 19 12 5 21 5 3"></polygon>
        </svg>

        <span className="ml-4">{text}</span>
      </button>
    </li>
  );
}
