import { useQueryStore } from "../../../store/query";
import Button from "../../ui/button";

export default function Toolbar() {
  const { tableData, query, saveQuery } = useQueryStore((state) => ({
    tableData: state.results,
    query: state.query,
    saveQuery: state.saveQuery,
  }));

  const handleSaveQueryClicked = () => {
    saveQuery(query);
  };

  const handleExportClicked = async () => {
    const { mkConfig, generateCsv, download } = await import("export-to-csv");

    const csvConfig = mkConfig({ useKeysAsHeaders: true });

    const csv = generateCsv(csvConfig)(tableData);

    download(csvConfig)(csv);
  };

  return (
    <div className="flex justify-between">
      <p>
        Showing {tableData.length} results for{" "}
        <code className="text-white bg-gray-800 p-1 rounded">{query}</code>.
        List is virtualized for performance.
      </p>
      <div className="pr-5 space-x-3">
        <Button onClick={handleExportClicked}>Export</Button>
        <Button onClick={handleSaveQueryClicked}>Save Query</Button>
      </div>
    </div>
  );
}
