import Header from "../header/header";
import Output from "./Output/output";
import QueryBox from "./QueryBox/querybox";

export default function Playground() {
  return (
    <div className="flex flex-1 p-5 flex-col max-w-[80%] max-h-screen">
      <Header />
      <QueryBox />
      <Output />
    </div>
  );
}
