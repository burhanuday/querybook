import { useQueryStore } from "../../../store/query";
import Button from "../../ui/button";

export default function QueryBox() {
  const runQuery = useQueryStore((state) => state.runQuery);

  const handleQuerySubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const query = event.currentTarget.query.value;

    runQuery(query);
  };

  return (
    <form onSubmit={handleQuerySubmit} className="w-full">
      <div className="flex space-x-4 w-full">
        <div className="flex-1 rounded-lg shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-indigo-600">
          <label htmlFor="query" className="sr-only">
            Query
          </label>
          <textarea
            required
            name="query"
            rows={5}
            id="query"
            className="block w-full resize-none border-0 bg-transparent py-1.5 text-gray-900 placeholder:text-gray-400 focus:ring-0"
            placeholder="Enter query here..."
          />
        </div>
        <div className="flex-0">
          <Button type="submit">Run Query</Button>
        </div>
      </div>
    </form>
  );
}
