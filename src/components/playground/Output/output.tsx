import { useMemo, useRef } from "react";
import { getTableHeaders } from "../../../utils/tableUtil";
import { useQueryStore } from "../../../store/query";
import {
  Row,
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  useReactTable,
} from "@tanstack/react-table";
import { useVirtual } from "@tanstack/react-virtual";
import Toolbar from "../Toolbar/toolbar";

export default function Output() {
  const { tableData } = useQueryStore((state) => ({
    tableData: state.results,
  }));

  const tableContainerRef = useRef<HTMLDivElement>(null);
  const columnHelper = createColumnHelper();

  const columns = useMemo(() => {
    const tableHeaders = getTableHeaders(tableData);

    return tableHeaders.map((header) => {
      return columnHelper.accessor(header, {
        header: () => header,
      });
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tableData]);

  const table = useReactTable({
    getCoreRowModel: getCoreRowModel(),
    data: tableData,
    // @ts-expect-error TODO: fix type error
    columns,
    debugTable: true,
  });

  const { rows } = table.getRowModel();
  const rowVirtualizer = useVirtual({
    parentRef: tableContainerRef,
    size: rows.length,
    overscan: 10,
  });
  const { virtualItems: virtualRows, totalSize } = rowVirtualizer;

  const paddingTop = virtualRows.length > 0 ? virtualRows?.[0]?.start || 0 : 0;
  const paddingBottom =
    virtualRows.length > 0
      ? totalSize - (virtualRows?.[virtualRows.length - 1]?.end || 0)
      : 0;

  return (
    <div className="mt-6">
      {tableData.length > 0 ? (
        <>
          <Toolbar />
          <div className="-mx-8 mt-2">
            <div className="block py-2 px-8">
              <div
                ref={tableContainerRef}
                className="shadow ring-1 ring-black ring-opacity-5 rounded-lg max-h-[500px] max-w-[900px] overflow-auto"
              >
                <table className="divide-y divide-gray-300 table-fixed border-collapse border-spacing-0">
                  <thead className="bg-gray-50">
                    {table.getHeaderGroups().map((headerGroup, index) => {
                      const isFirst = index === 0;

                      return (
                        <tr key={headerGroup.id}>
                          {headerGroup.headers.map((header) => {
                            return (
                              <th
                                key={header.id}
                                className={`px-3 py-3.5 text-left text-sm font-semibold text-gray-900 capitalize ${
                                  isFirst ? "pl-4" : ""
                                }`}
                              >
                                {header.isPlaceholder
                                  ? null
                                  : flexRender(
                                      header.column.columnDef.header,
                                      header.getContext()
                                    )}
                              </th>
                            );
                          })}
                        </tr>
                      );
                    })}
                  </thead>
                  <tbody className="divide-y divide-gray-200 bg-white">
                    {paddingTop > 0 && (
                      <tr>
                        <td style={{ height: `${paddingTop}px` }} />
                      </tr>
                    )}
                    {virtualRows.map((virtualRow) => {
                      const row = rows[virtualRow.index] as Row<unknown>;
                      return (
                        <tr key={row.id}>
                          {row.getVisibleCells().map((cell, index) => {
                            const isFirst = index === 0;

                            return (
                              <td
                                key={cell.id}
                                className={`whitespace-nowrap px-3 py-4 text-sm ${
                                  isFirst ? "pl-4" : ""
                                } text-gray-500`}
                              >
                                {flexRender(
                                  cell.column.columnDef.cell,
                                  cell.getContext()
                                )}
                              </td>
                            );
                          })}
                        </tr>
                      );
                    })}
                    {paddingBottom > 0 && (
                      <tr>
                        <td style={{ height: `${paddingBottom}px` }} />
                      </tr>
                    )}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </>
      ) : null}
    </div>
  );
}
