import Playground from "./components/playground/playground";
import Sidebar from "./components/sidebar/sidebar";

function App() {
  return (
    <main className="h-screen w-screen flex flex-col overflow-hidden">
      <section className="flex flex-1">
        <Sidebar />
        <Playground />
      </section>
    </main>
  );
}

export default App;
