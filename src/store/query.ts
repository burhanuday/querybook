import { create } from "zustand";
import { getDataFromQuery } from "../utils/queryUtil";

export const useQueryStore = create<{
  query: string;
  results: Record<string, string | number>[];
  history: string[];
  runQuery: (query: string) => void;
  savedQueries: string[];
  saveQuery: (query: string) => void;
}>((set) => ({
  query: "",
  savedQueries: [
    "SELECT * FROM users;",
    "SELECT * FROM posts;",
    "SELECT * FROM comments;",
  ],
  results: [],
  history: [],
  runQuery: (query) => {
    const results = getDataFromQuery();

    set((state) => ({
      ...state,
      results,
      history: [query, ...state.history],
      query,
    }));
  },
  saveQuery: (query: string) => {
    set((state) => ({
      ...state,
      savedQueries: [query, ...state.savedQueries],
    }));
  },
}));
