export const getTableHeaders = (
  tableData: Array<Record<string, string | number>>
) => {
  return tableData.length > 0 ? Object.keys(tableData[0]) : [];
};

export const getTableRows = (
  tableData: Array<Record<string, string | number>>
) => {
  return tableData.map((row) => Object.values(row));
};
