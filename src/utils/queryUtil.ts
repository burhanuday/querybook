import * as mockData from "../assets/mockData";

let lastExportIndex = 0;

// return the next export from mockData
export const getDataFromQuery = (): Record<string, string | number>[] => {
  const exports = Object.keys(mockData);
  const nextExport = exports[lastExportIndex++ % exports.length];

  // @ts-expect-error - this is a mock, so we don't care about the type
  return mockData[nextExport];
};
